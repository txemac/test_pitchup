Pitchup
=======

# Description

Write a program to find a pitch based on this clue: "Join us at pitch x.
x is a number between 1 and 553 such that the sum of x's divisors (not
including x) is greater than x but no subset of x's divisors add up to
exactly x."


# Execute
    python pitchup.py


# Author
Jose Bermudez
info@josebermudez.co.uk
